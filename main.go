package main

import (
	"fmt"
	"path"
	"hash/crc32"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		log.Fatal("expecting an argument which points to a file")
	}

	filename := os.Args[1]

	bytes, err := ioutil.ReadFile(filename)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%08X %s\r\n", crc32.ChecksumIEEE(bytes), path.Base(filename))
}
